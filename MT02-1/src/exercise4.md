# Чек-лист

## Тест-кейс №1. Авторизация пользователя
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, неожиданное поведение приложения: невозможность авторизации пользователя, блокирующий баг, тестирование невозможно.`**

problem_user
**`Результат: Провален. Неожиданное поведение приложения: Авторизация успешна, на странице каталога товаров изображения некорректны.`**

performance_glitch_user
**`Результат: пройден не полностью, присутствует задержка перехода на страницу каталога.`**

## Тест-кейс №2. Кнопка добавления товара в корзину
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №3 - Кнопка удаления товара из корзины на странице каталога
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка перехода на страницу.`

### Тест-кейс №4 - Переход в корзину
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №5 - Оформление заказа
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
**`Результат: провален, нет возможности ввести в поле "Last name" данные для оформления заказа, введённые символы добавляются в "First name"`**

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №6 - подтверждение заказа
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
**`Результат: провален, тестирование невозможно из-за невозможности ввести данные в поле "Last Name", без данного поля выдаёт ошибку "Error: Last Name is required"`**

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №7 Отмена покупки  на странице подтверждения
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
**`Результат: провален.`**

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка перехода на страницу.`

### Тест-кейс №8 - отмена покупки на странице оформления
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
**`Результат: провален.`**

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка перехода на страницу.`

### Тест-кейс №9 - Удаление товара в корзине и выход из корзины
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка перехода на страницу.`

### Тест-кейс №10 - Кнопка добавления товара в корзину на странице карточки товара
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1..`**

problem_user
**`Результат: провален, кнопка "Add to cart" неактивна, добавить товар в корзину нет возможности.`**

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №11 - Открытие бокового меню, кнопка "Гамбургер"
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №12 - Переход на страницу каталога из бокового меню
Standart_user
`Результат: пройден.`

problem_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка перехода на страницу.`

### Тест-кейс №13 - Переход на сайт разработчиков
Standart_user
`Результат: пройден.`

problem_user
**`Результат: провален, переход происходит на подстраницу сайта разработчика с ошибкой 404.`**

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №14 - выход из профиля через боковое меню
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №15 - Переход на внешние ресурсы из подвала сайта

Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
`Результат: пройден.`

performance_glitch_user
`Результат: пройден.`

### Тест-кейс №16 - сортировка каталога с помощью бокового меню под "корзиной"
Standart_user
`Результат: пройден.`

locked_out_user
**`Результат: провален, авторизация пользователя невозможна в соответствии с тест-кейсом №1.`**

problem_user
**`Результат: провален, нет возможности поменять/выбрать сортировку каталога.`**

performance_glitch_user
`Результат: пройден не полностью, присутствует задержка при смене типа сортировки.`