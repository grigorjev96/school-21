### Попарное тестирование

| Столбец 1 | Столбец 2 |
|----------|----------|
Пустой	|Пустой|
Пустой	|Неверное значение|
Пустой	|secret_sauce|
Неверное значение	|Неверное значение|
Неверное значение	|secret_sauce|
Неверное значение	|Пустой|
standard_user	|secret_sauce|
standard_user	|Пустой|
standard_user	|Неверное значение|
locked_out_user	|Пустой|
locked_out_user	|Неверное значение|
locked_out_user	|secret_sauce|
problem_user	|Неверное значение|
problem_user	|secret_sauce|
problem_user	|Пустой|
performance_glitch_user	|secret_sauce|
performance_glitch_user	|Пустой|
performance_glitch_user	|Неверное значение|
___
**Тест-кейс №1 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" ничего не вводить
- В поле "Password" ничего не вводить
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Username is required"
___
**Тест-кейс №2 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" Ничего не вводить
- В поле "Password" Неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Username is required"
___
**Тест-кейс №3 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" Ничего не вводить
- В поле "Password" Верное значение пароля secret_sauce
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Username is required"
___
**Тест-кейс №4 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" Неверное значение
- В поле "Password" Неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Username and password do not match any user in this service"
___
**Тест-кейс №5 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" Неверное значение
- В поле "Password" Верное значение Secret_sauce
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Username is required"
___
**Тест-кейс №6 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" Неверное значение
- В поле "Password" Ничего не вводить
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №7 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" standart_user
- В поле "Password" Secret_sauce
3. Ожидаемый результат
- Авторизация пройдена успешно
___
**Тест-кейс №8 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" standart_user
- В поле "Password" Пустое значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №9 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" standart_user
- В поле "Password" неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №10 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" locked_out_user
- В поле "Password" secret_sauce
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Sorry, this user has been locked out."
___
**Тест-кейс №11 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" locked_out_user
- В поле "Password" Пустое значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №12 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" locked_out_user
- В поле "Password" Неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №13 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" problem_user
- В поле "Password"secret_sauce
3. Ожидаемый результат
- Авторизация пройдена успешно
___
**Тест-кейс №14 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" problem_user
- В поле "Password" Пустое значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №15 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" problem_user
- В поле "Password" Неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №16 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" performance_glitch_user
- В поле "Password" Secret_sauce
3. Ожидаемый результат
- Авторизация пройдена успешно
___
**Тест-кейс №17 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" performance_glitch_user
- В поле "Password" Пустое значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"
___
**Тест-кейс №18 - проверка авторизации**
1. Условие
- Открыта страница авторизации https://www.saucedemo.com/ 
2. Шаги:
- В поле "Username" performance_glitch_user
- В поле "Password" Неверное значение
3. Ожидаемый результат
- Ошибка авторизации "Epic sadface: Password is required"