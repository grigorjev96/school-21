# Задание 3.2 XPath
|<!-- -->  |<!-- -->  |
|:-----|----:|
|**Что будет застраховано**|
| Кнопка "Квартира" | //*[@id="mat-button-toggle-1-button"] |
|Кнопка "Дом" | //*[@id="mat-button-toggle-2"] |
|**Где находится недвижимость**|
| Поле ввода "Регион проживания" | //*[@id="mat-input-0"] |
|**Срок действия страхования**|
|Поле ввода даты сроков действия страхования| //*[@id="mat-input-1"] |
|Выбор месяца и года в календаре сроков действия страхования| //*[@id="mat-datepicker-0"]/mat-calendar-header/div/div/button[1]|
|Перевод календаря на следующий месяц| //*[@id="mat-datepicker-0"]/mat-calendar-header/div/div/button[3]|
|Перевод календаря на предыдущий месяц |//*[@id="mat-datepicker-0"]/mat-calendar-header/div/div/button[2]|
|Выбор дня в месяце| //*[@id="mat-datepicker-0"]/div/mat-month-view/table/tbody/tr[2]/td[6]/div[1]|
|Выбор года в календаре| //*[@id="mat-datepicker-0"]/div/mat-multi-year-view/table/tbody/tr[1]/td[1]/div[1]|
|**Особенности объекта**|
|Сдается в аренду, кнопка "Да"| //*[@id="mat-button-toggle-22-button"]|
|Сдается в аренду, кнопка "Нет"| //*[@id="mat-button-toggle-50-button"]|
|Расположена на первом или последнем этаже, кнопка "Да" |//*[@id="mat-button-toggle-52-button"]|
|Расположена на первом или последнем этаже, Кнопка "Нет"| //*[@id="mat-button-toggle-53-button"]|
|Установлена охранная сигнализация, кнопка "Да"| //*[@id="mat-button-toggle-55-button"]|
|Установлена охранная сигнализация, кнопка "Нет"| //*[@id="mat-button-toggle-56-button"]|
|Материал несущих стен, кнопка "кирпич или монолит"| //*[@id="mat-button-toggle-64-button"]|
|Материал несущих стен, кнопка "дерево"| //*[@id="mat-button-toggle-65-button"]|
|**Страховая сумма и объекты страхования**|
|Поле ввода суммы |//*[@id="mat-input-3"] |
|Слайдер для выбора суммы |//*[@class='mat-slider-wrapper']|
|**Промокод**|
|Поле для ввода промокода| //*[@id="mat-input-2"]|
|Кнопка "Применить"| /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[9]/div[2]/div/button|
|**Оформление**|
|Кнопка "Оформить"| /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[2]/button|
|**Кнопка одобрения обработки ПД**|
|Кнопка "Принять" |/html/body/app-root/app-cookie-agreement/div/button|
|**Страхователь**|
|Кнопка "Заполнить по Сбер ID"| /html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[1]/button|
|Поле ввода фамилии |//*[@id="mat-input-4"]|
|Поле ввода имени |//*[@id="mat-input-5"]|
|Поле ввода отчества| //*[@id="mat-input-6"]|
|Чекбокс "Отчество отсутствует"| //*[@id="mat-checkbox-1"]/label/div|
|Поле ввода "Даты рождения" |//*[@id="mat-input-7"]|
|Кнопка "Мужской" |//*[@id="mat-button-toggle-76-button"]|
|Кнопка "Женский"| //*[@id="mat-button-toggle-77-button"]|
|**Паспортные данные Страхователя**|
|Поле ввода "Серии"| //*[@id="mat-input-8"]|
|Поле ввода "Номера"| //*[@id="mat-input-9"]|
|Поле ввода "Даты выдачи"| //*[@id="mat-input-10"]|
|Поле ввода "Кем выдан"| //*[@id="mat-input-11"]|
|Поле ввода "Код подразделения"| //*[@id="mat-input-12"]|
|**Адрес имущества**|
|Поле выбора "Регион"| //*[@id="mat-input-13"]|
|Поле ввода Город или населенный пункт|//*[@id="mat-input-14"]|
|Поле ввода Улица|//*[@id="mat-input-15"]|
|Поле ввода Дом, литера, корпус, строение|//*[@id="mat-input-16"]|
|Поле ввода Квартира|//*[@id="mat-input-17"]|
|Чекбокс "Улица отсутствует"|//*[@id="mat-checkbox-2"]/label/div|
|**Контактные данные страхователя**||
|Поле ввода телефон|//*[@id="mat-input-18"]|
|Поле ввода электронной почты|//*[@id="mat-input-19"]|
|Поле ввода для повтора электронной почты|//*[@id="mat-input-20"]|
|Кнопка "Вернуться"|/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[5]/button[1]|
|Кнопка "Оформить"|/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[5]/button[2]|

