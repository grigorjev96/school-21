## 1 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200 

Тело ответа (при наличии). Если тело большое, то только его часть; 

        {
        "id": 1,
        "title": "Activity 1",
        "dueDate": "2023-06-27T12:46:48.460623+00:00",
        "completed": false
        },
---
## 2 Эндпоинт
HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    {
     "id": 0,
     "title": "string",
     "dueDate": "2023-06-27T11:48:49.187Z",
     "completed": true
    }

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    {
    "id": 0,
    "title": "string",
    "dueDate": "2023-06-27T11:48:53.685Z",
    "completed": true
    }
---
## 3 Эндпоинт
HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    {
    "id": 0,
    "title": 
    "dueDate": "2023-06-27T11:48:49.187Z",
    "completed": true
    }

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    {
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-df197b0330083b43ae2d536d326ede39-01cd442ba189cf45-00",
    "errors": {
    "$": [
      "':' is invalid after a value. Expected either ',', '}', or ']'. Path: $ | LineNumber: 3 | BytePositionInLine: 11."
    ]
    }

---
## 4 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); - 

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-06-27T12:53:54.0686245+00:00",
    "completed": false
    
___
## 5 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities/-1


Заголовки запроса; -H  "accept: text/plain; v=1.0

Тело запроса (при наличии); - 

Статус-код ответа; 404

Тело ответа (при наличии). Если тело большое, то только его часть;

    
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "00-fd3ffb045177964cb0e70c06211d2ad0-a1b3920905106a46-00"
    
___
## 6 Эндпоинт
HTTP-метод; PUT 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities/1


Заголовки запроса;  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    
     "id": 0,
     "title": "string",
     "dueDate": "2023-06-27T12:00:47.500Z",
     "completed": true
    

Статус-код ответа; 200 

Тело ответа (при наличии). Если тело большое, то только его часть;

    
     "id": 0,
     "title": "string",
     "dueDate": "2023-06-27T12:00:47.5Z",
     "completed": true
    
___
## 7 Эндпоинт

HTTP-метод; PUT

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities/-1


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id":
    "title": "string",
    "dueDate": "2023-06-27T12:00:47.500Z",
    "completed": true

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
     "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-f64d3461a2cb8349a0cec743329d8113-4676a4686562c94a-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 9."
___
## 8 Эндпоинт

HTTP-метод; DELETE

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Activities/1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;
___
## 9 Эндпоинт

HTTP-метод; GET https://fakerestapi.azurewebsites.net/api/v1/Authors


Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
___

## 10 Эндпоинт

HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

        "id": 0,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

        "id": 0,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"
___

## 11 Эндпоинт

HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id": 0,
    "idBook": 
    "firstName": "string",
    "lastName": "string"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-72dab9497cfce84eafb82331d2b25114-f7829251243c3c49-00",
    "errors": 
        "$.idBook": 
        "The JSON value could not be converted to System.Int32. Path: $.idBook | LineNumber: 3 | BytePositionInLine: 13."
___
## 12 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200 

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
     },
     {
    "id": 2,
    "idBook": 1,
    "firstName": "First Name 2",
    "lastName": "Last Name 2"
___
## 13 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/99999999999


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); - 

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть; 

        "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-8982f0e58dcf6c46be821b20136c4025-b4f9bcec3c072645-00",
    "errors": {
        "idBook": [
        "The value '99999999999' is not valid."
___
## 14 Эндпоинт

HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); - 

Статус-код ответа; 200 

Тело ответа (при наличии). Если тело большое, то только его часть;

        "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
___
## 15 Эндпоинт
HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 404

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "00-f8bcb852f6f7614e98668ee790ee5100-04d3eeeb19546e4b-00"
___
## 16 Эндпоинт
HTTP-метод; PUT 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/1


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 0,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 0,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"
___
## 17 Эндпоинт
HTTP-метод; PUT 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/1


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id":
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-d5c54b0d4f552449837432eb2f7a0453-cf9288742509eb41-00",
    "errors": {
        "$.id": [
        "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 10."
___
## 18 Эндпоинт
HTTP-метод; DELETE

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Authors/1


Заголовки запроса;-H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; -
___
## 19 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-26T12:34:25.8015342+00:00"
___
## 20 Эндпоинт
HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d 

Тело запроса (при наличии); 

    "id": 0,
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:48:55.472Z"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 0,
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:48:55.472Z"
___
## 21 Эндпоинт
HTTP-метод; POST

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id": 
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:48:55.472Z"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-15fee8fbfbbc2942909961e3cb224e31-acbb90e315bf4e4e-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 9."
__
## 22 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-26T12:51:13.7584885+00:00"
___
## 23 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books/-1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 404 

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "00-a8af989be900804abbaf10aa18ea0de7-e65bc18cf446cf46-00"
___
## 24 Эндпоинт
HTTP-метод; PUT

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books/1


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 0,
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:52:47.888Z"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 0,
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:52:47.888Z"
___
## 25 Эндпоинт
HTTP-метод; PUT 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books/1


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 
    "title": "string",
    "description": "string",
      "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-06-27T12:52:47.888Z"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-abbe49ff77a3274e9c5283a6d7929cc4-0cdf66181ce18646-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 9."
___
## 26 Эндпоинт

HTTP-метод; DELETE

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Books/1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; -
___
## 27 Эндпоинт
HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
___
## 28 Эндпоинт
HTTP-метод; POST 

Полный URL запроса;  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 0,
    "idBook": 0,
    "url": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 0,
    "idBook": 0,
    "url": "string"
___
## 29 Эндпоинт
HTTP-метод; POST 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);


    "id": 
    "idBook": 0,
    "url": "string"


Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-ce393cb14cb1ef4f8b1eb11b60dcf491-21d5d3ecee0a6a43-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 10."
___
## 30 Эндпоинт
HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
___
## 31 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/1


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-e1c5fa893ea40e4ab8f841737cd65494-7b4c7e686b1ae542-00",
    "errors": {
    "idBook": [
      "The value '99999999999' is not valid."
___
## 32 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1


Заголовки запроса;  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
___
## 33 Эндпоинт
HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1

Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 404

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "00-662bda4e9377024498cf931ce71107b1-4d8435df36d5df40-00"
___
## 34 Эндпоинт
HTTP-метод; PUT
 
Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1


Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 0,
    "idBook": 0,
    "url": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

  "id": 0,
  "idBook": 0,
  "url": "string"
___
## 35 Эндпоинт
HTTP-метод; PUT

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1

Заголовки запроса; -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id": 
    "idBook": 0,
    "url": "string"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-1fdd61770eea1540a05c96517a365b56-209b4b7889f4e844-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 10."
___
## 36 Эндпоинт
HTTP-метод; DELETE 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; -
___
## 37 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users


Заголовки запроса; -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; 

    "id": 1,
    "userName": "User 1",
    "password": "Password1"
___
## 38 Эндпоинт
HTTP-метод; POST 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id": 0,
    "userName": "string",
    "password": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 0,
    "userName": "string",
    "password": "string"
___
## 39 Эндпоинт
HTTP-метод; POST 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users

Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id":
    "userName": "string",
    "password": "string"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-dfd541222dd78240b895621a0885b1ef-77ad70acf2644f43-00",
    "errors": {
     "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 12."
____
## 40 Эндпоинт
HTTP-метод; GET 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users/1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 1,
    "userName": "User 1",
    "password": "Password1"
___
## 41 Эндпоинт
HTTP-метод; GET

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users/-1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 404 

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
     "status": 404,
     "traceId": "00-c833add136be8f47a83ad197a3db8a6c-51770ed61959e54b-00"
___
## 42 Эндпоинт
HTTP-метод; PUT

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users/1


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии);

    "id": 0,
    "userName": "string",
    "password": "string"

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть;

    "id": 0,
    "userName": "string",
    "password": "string"
___
## 43 Эндпоинт
HTTP-метод; PUT 

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users/1


Заголовки запроса; -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии); 

    "id":
    "userName": "string",
    "password": "string"

Статус-код ответа; 400

Тело ответа (при наличии). Если тело большое, то только его часть;

    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-a6707df52feaed48b90febeb9150f97b-b323267c3db6294d-00",
    "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 12."
___
## 44 Эндпоинт
HTTP-метод; DELETE

Полный URL запроса; https://fakerestapi.azurewebsites.net/api/v1/Users/1


Заголовки запроса; -H  "accept: */*"

Тело запроса (при наличии); -

Статус-код ответа; 200

Тело ответа (при наличии). Если тело большое, то только его часть; -