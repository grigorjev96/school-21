# Алгоритмы балансировки нагрузки

## Round Robin
- Самый простой способ снизить нагрузку на сервер — это отправлять каждый запрос поочередно. Предположим, у вас есть три VDS. Запросы направляются поочередно на первый, второй, а затем третий сервер. Следующий запрос снова вернется к первому серверу, и цикл начнется заново. Плюсы подхода очевидны: простота, дешевизна, эффективность. При этом серверы из пула могут не быть связаны между собой — через DNS и этот алгоритм можно перенаправлять запросы на любые машины. Главная проблема этого подхода — нерациональное распределение ресурсов. Даже если все машины обладают примерно одинаковыми характеристиками, реальная нагрузка будет сильно различаться в пуле.

Особенности: Прост в реализации, обеспечивает равномерное распределение нагрузки между серверами.

Недостатки: Не учитывает нагрузку и производительность серверов. Если у серверов разная производительность, некоторые серверы могут стать узкими местами.

## Weighted Round Robin
- Это — усовершенствованная версия алгоритма Round Robin. Суть усовершенствований заключается в следующем: каждому серверу присваивается весовой коэффициент в соответствии с его производительностью и мощностью. Это помогает распределять нагрузку более гибко: серверы с большим весом обрабатывают больше запросов. Однако всех проблем с отказоустойчивостью это отнюдь не решает. Более эффективную балансировку обеспечивают другие методы, в которых при планировании и распределении нагрузки учитывается большее количество параметров.

Особенности: Позволяет учесть разную производительность серверов и задать им различные веса.

Недостатки: Веса должны быть заданы заранее, и изменение весов требует изменения конфигурации системы. Если веса неправильно настроены, может возникнуть дисбаланс нагрузки.

## Sticky Sessions
- алгоритм распределения входящих запросов, при котором соединения передаются на один и тот же сервер группы. Он используется, например, в веб-сервере Nginx. Сессии пользователя могут быть закреплены за конкретным сервером с помощью метода IP hash (подробную информацию о нём см. в официальной документации). С помощью этого метода запросы распределяются по серверам на основе IP-aдреса клиента. Как указано в документации (см. ссылку выше), «метод гарантирует, что запросы одного и того же клиента будет передаваться на один и тот же сервер». Если закреплённый за конкретным адресом сервер недоступен, запрос будет перенаправлен на другой сервер. Применение этого метода сопряжено с некоторыми проблемами. Проблемы с привязкой сессий могут возникнуть, если клиент использует динамический IP. В ситуации, когда большое количество запросов проходит через один прокси-сервер, балансировку вряд ли можно назвать эффективной и справедливой

Особенности: Сохранение состояния клиента: Sticky Sessions позволяют серверу сохранить состояние клиента, что полезно для приложений, которые требуют последовательных запросов от одного клиента (например, состояние сеанса, данные авторизации и т. д.).

Недостатки:
Неравномерное распределение нагрузки: Sticky Sessions нарушает равномерное распределение нагрузки, поскольку запросы от одного клиента всегда направляются на один сервер. Это может привести к неравномерной нагрузке на серверы, особенно если клиенты не равномерно распределены между серверами.

## Least Connections
- Алгоритм, учитывающий количество подключений к серверу. Каждый поступивший запрос отправляется серверу с наименьшим количеством активных подключений. Уязвимое место — необходимость балансировки между несколькими Frontend-серверами. Когда пользователь устанавливает соединение с Frontend-сервером, все запросы будут отправляться именно на него. При соблюдении алгоритма Least connections другой Frontend-сервер может быть менее нагружен, и пользователь подключится к нему — ему придется заново авторизоваться. 

Особенности: Распределяет нагрузку на серверы с учетом текущей загрузки.

Недостатки: Требует мониторинга состояния серверов и подсчета активных соединений. Не учитывает различия в производительности серверов и может привести к ситуации, когда нагрузка неравномерно распределена из-за отличий в обработке запросов.

## Hash-Based
- Алгоритм хэширования (Hash-based): Запросы хэшируются, и результат хеш-функции используется для выбора сервера, на который будет направлен запрос. Этот алгоритм гарантирует, что один и тот же клиент будет каждый раз направляться на один и тот же сервер.

Особенности: Обеспечивает постоянное направление запросов от одного клиента на один и тот же сервер.

Недостатки: Не учитывает текущую нагрузку серверов и может привести к неравномерному распределению нагрузки, особенно если хеш-функция плохо равномерно распределяет запросы.

## Least Response Time
Метод наименьшей задержки (Least Response Time): Запросы направляются на сервер с наименьшей задержкой ответа. Измеряется время от отправки запроса до получения ответа от сервера. Этот алгоритм пытается минимизировать общую задержку обработки запросов.

Особенности: Учитывает задержку ответа серверов и направляет запросы на сервер с наименьшей задержкой.

Недостатки: Требует измерения задержки ответа для каждого сервера, что может быть ресурсоемкой операцией. Если задержка меняется со временем, алгоритм может становиться неэффективным.