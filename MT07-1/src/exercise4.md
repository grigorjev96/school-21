## Запрос GET к Activities

URL "https://fakerestapi.azurewebsites.net/api/v1/Activities"

Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

Заголовки запроса: ```Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0d89d8c0-b4b4-4f60-810f-883a74d70b5a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive```

Тело запроса: Отсутствует

Заголовки ответа ``` Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:06:18 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0 ```

Тело ответа 

```{"id":1,"title":"Activity 1","dueDate":"2023-06-30T11:06:19.3601373+00:00","completed":false}```
___

## Запрос POST к Activities

URL "https://fakerestapi.azurewebsites.net/api/v1/Activities"

Ожидаемый результат Ответ от сервера с 200 статус кодом и запись ID в список

Заголовки запроса: ```Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: eb17f2ef-b10f-4281-9297-3a6def2c77f2
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101```

Тело запроса: 
{
     "id": 0,
     "title": "string",
    "dueDate": "2023-06-29T23:26:22.735Z",
     "completed": true
}

Заголовки ответа ``` Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:06:18 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0 ```

Тело ответа 

```{"id":0,"title":"string","dueDate":"2023-06-29T23:26:22.735Z","completed":true}```
___
## Запрос POST к Activities без тела

URL "https://fakerestapi.azurewebsites.net/api/v1/Activities"

Ожидаемый результат Ответ от сервера с 400 статус кодом и отчет об ошибке

Заголовки запроса: ```Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 8211deb6-d008-4032-b53a-986d0dfa5198
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0```

Тело запроса: Отсутствует

Заголовки ответа ``` Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 10:44:44 GMT
Server: Kestrel
Transfer-Encoding: chunked ```

Тело ответа 

```{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-db3b6056f5727142b7add2c7c93096c2-bca8c05f7ac8dc45-00","errors":{"":["A non-empty request body is required."]}}```
___
## Запрос GET к Activities с использованием ID

URL https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":1,"title":"Activity 1","dueDate":"2023-06-30T11:47:36.3636608+00:00","completed":false}`
___
## Ошибочный Запрос GET к Activities с использованием несуществующего ID

URL https://fakerestapi.azurewebsites.net/api/v1/Activities/31

Ожидаемый результат: Ответ от сервера с 404 статус кодом и информацией по ошибке

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-0e5ac78cc513a84586f2bdf2d97e772d-18520c23f11cd244-00"}`
___
## Ошибочный Запрос GET к Activities с использованием несуществующего ID

URL https://fakerestapi.azurewebsites.net/api/v1/Activities/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-3349613fd9e1ef468620c2f7c85444e8-b13559e123a92645-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
## Запрос PUT к Activities с использованием ID

URL https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101``

Тело запроса: `{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-29T23:28:36.643Z",
  "completed": true
}`

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":0,"title":"string","dueDate":"2023-06-29T23:28:36.643Z","completed":true}`
___
## Ошибочный Запрос PUT к Activities с использованием неправильного ID

URL: {{baseurl}}/api/v1/Activities/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101``

Тело запроса: `{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-29T23:28:36.643Z",
  "completed": true
}`

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-1a8c742b0148644c8489f9a5f241e7a4-e4ccd486ee1ea64f-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
## Ошибочный Запрос PUT к Activities без использования тела запроса

URL: {{baseurl}}/api/v1/Activities/1

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: ``Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-e754683bfe40b84c9a9b0a24b218e77c-1c1ace0d7ea0914e-00","errors":{"":["A non-empty request body is required."]}}`
___
## Запрос DELETE к Activities с использованием ID

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: ``Content-Type: application/json
Accept: *|*
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: Отсутствует
___

## Ошибочный Запрос DELETE к Activities с использованием неправильного ID

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом

Заголовки запроса: ``Content-Type: application/json
Accept: *|*
User-Agent: PostmanRuntime/7.32.3
Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101``

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 10:47:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-59fc4dd7b4cace4cb5b455e688aa4bba-61f1a67dea77ad4d-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
_____
## Запрос GET к Authors 

URL: https://fakerestapi.azurewebsites.net/api/v1/authors

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 50d56802-0ede-4af6-9e72-3d2b8ce562a0
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 11:49:52 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"},{"id":2,"idBook":1,"firstName":"First Name 2","lastName":"Last Name 2"}`
_____
## Запрос POST к Authors 

URL: https://fakerestapi.azurewebsites.net/api/v1/authors

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 82`

Тело запроса: `{
    "id": 0,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"
}`

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 11:49:52 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":0,"idBook":0,"firstName":"string","lastName":"string"}`
___
## Ошибочный Запрос POST к Authors без тела запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/authors

Ожидаемый результат: Ответ от сервера с 400 статус кодом и описанием ошибки

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 11:59:23 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-64061a937c963342ad2178d99ee68225-5225ddfdfce5404f-00","errors":{"":["A non-empty request body is required."]}}`
___
##  Запрос GET к Authors/books с использованием book_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{book_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `[{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"},{"id":2,"idBook":1,"firstName":"First Name 2","lastName":"Last Name 2"},{"id":3,"idBook":1,"firstName":"First Name 3","lastName":"Last Name 3"},{"id":4,"idBook":1,"firstName":"First Name 4","lastName":"Last Name 4"}]`
___
##  Ошибочный Запрос GET к Authors/books с использованием book_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/201

Ожидаемый результат: Ответ от сервера с 404 статус кодом 

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `[]`
___
## Ошибочный Запрос GET к Authors/books с использованием book_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом 

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-95ec2a0dadfb1d429060f8d67da4c887-65466d1908cec447-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
##  Запрос GET к Authors с использованием author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{{author_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"}`
___
##  Ошибочный Запрос GET к Authors с использованием неккоректного author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/650

Ожидаемый результат: Ответ от сервера с 404 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-efc5f566eb368a4d93c18bd9ef50ebe4-b260499d9ae00449-00"}`
___
##  Ошибочный Запрос GET к Authors с использованием неккоректного author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/650

Ожидаемый результат: Ответ от сервера с 404 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-efc5f566eb368a4d93c18bd9ef50ebe4-b260499d9ae00449-00"}`
___
##  Ошибочный Запрос GET к Authors с использованием неккоректного author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0e4d326c-bf42-4b6e-af9e-36cb706ee352
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-454acbd35ed631409fda5d186c9feb3f-8fbdd2a770f33345-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
## Запрос PUT к Authors с использованием author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{{author_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cd114f45-68e8-43cc-aacc-cbae959ce462
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 82`

Тело запроса: `{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}`

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":0,"idBook":0,"firstName":"string","lastName":"string"}`
___
## Ошибочный Запрос PUT к Authors с использованием неккоректного author_id

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cd114f45-68e8-43cc-aacc-cbae959ce462
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 82`

Тело запроса: `{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}`

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-1056308c06cd53489119873181ba1dd5-8212101ba23a0849-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
## Ошибочный Запрос PUT к Authors с отсутствующим телом запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{{author_id}}

Ожидаемый результат: Ответ от сервера с 400 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cd114f45-68e8-43cc-aacc-cbae959ce462
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 82`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 12:01:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-1fa6fb8ff4c43b4cac8132045ec19258-7457d531e1a0ed4e-00","errors":{"":["A non-empty request body is required."]}}`
___
## Запрос DELETE к Authors 

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{{author_id}}

Ожидаемый результат: Ответ от сервера с 200 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cd114f45-68e8-43cc-aacc-cbae959ce462
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Length: 0
Date: Fri, 30 Jun 2023 12:32:11 GMT
Server: Kestrel
api-supported-versions: 1.0`

Тело ответа: Отсутствует
___
## Запрос DELETE к Authors с использованием неккоректного ID

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{{author_id}}

Ожидаемый результат: Ответ от сервера с 400 статус кодом и телом ответа

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cd114f45-68e8-43cc-aacc-cbae959ce462
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 12:34:51 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-bf5365dc72ebc24795785e090f389efc-711ef0396891cd4e-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___

## Запрос GET к Books

 URL https://fakerestapi.azurewebsites.net/api/v1/Books

 Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

 Заголовки запроса:`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 5dc3380e-728d-4b1e-86d0-be3ba170dfc8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`
Тело запроса: Отсутствует

 Заголовки ответа:`Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 09:49:38 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0 `


 Тело ответа: ` {"id":1,"title":"Book 1","description":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","pageCount":100,"excerpt":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","publishDate":"2023-06-30T09:49:38.9945955+00:00"},`

## Запрос POST к Books 

URL https://fakerestapi.azurewebsites.net/api/v1/Books

Ожидаемый результат Ответ от сервера с 200 статус кодом и запись ID в список

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: a77467ff-0968-4510-b178-877ebec4dcff
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 154`

Тело запроса:`
{
    "id": 0,
    "title": "string",
    "description": "string",
    "pageCount": 0,
    "excerpt": "string",
    "publishDate": "2023-07-01T09:41:17.439Z"
}`

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 09:57:48 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа `{"id":0,"title":"string","description":"string","pageCount":0,"excerpt":"string","publishDate":"2023-07-01T09:41:17.439Z"}`

## Запрос POST к Books без тела 

URL "https://fakerestapi.azurewebsites.net/api/v1/Books 

Ожидаемый результат Ответ от сервера с 400 статус кодом и отчет об ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: a77467ff-0968-4510-b178-877ebec4dcff
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 09:57:48 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа
`{"id":0,"title":"string","description":"string","pageCount":0,"excerpt":"string","publishDate":"2023-07-01T09:41:17.439Z"}`

## Запрос GET к Books с использованием ID 

URL https://fakerestapi.azurewebsites.net/api/v1/books/1

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: a657280c-5c77-44f2-b439-1abaa7ff81f8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 10:09:08 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"id":1,"title":"Book 1","description":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","pageCount":100,"excerpt":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","publishDate":"2023-06-30T10:09:08.8794919+00:00"},`

## Ошибочный Запрос GET к Books с использованием несуществующего ID

URL https://fakerestapi.azurewebsites.net/api/v1/Users/31

Ожидаемый результат: Ответ от сервера с 404 статус кодом и информацией по ошибке
Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: abc18d9d-5aa2-4941-8454-cc121c62db7f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 10:15:18 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-8bcb99021cf1d04593ac662c5eb2ee35-46144664c428fc46-00"}`

## Ошибочный Запрос GET к Books с использованием несуществующего ID

URL https://fakerestapi.azurewebsites.net/api/v1/Books/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса:`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 8113a8b3-73dd-4792-b4d0-f53ed4d2189f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, defla
te, be
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 10:19:31 GMT
Server: Kestrel
Transfer-Encoding: chunked`
Тело ответа:
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-6339173deabc6042a239f3d9ac17ba45-18dd249ab2325347-00","errors":{"id":["The value '99999999999999\n' is not valid."]}}`

## Запрос PUT к Books с использованием ID
URL https://fakerestapi.azurewebsites.net/api/v1/Books/0

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 644df7d1-b127-4570-a53c-70b9b55ad16a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: `{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-07-01T10:28:50.677Z"`
}

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 10:27:39 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-85122d5cfbdc994585f2f77661c7eb3b-87b24ab1c72c9a4e-00","errors":{"":["A non-empty request body is required."]}}`

## Ошибочный Запрос PUT к Books с использованием неправильного ID
URL: https://fakerestapi.azurewebsites.net/api/v1/Books/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке
Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 42530a1a-720d-4aed-ae81-ec4e86ba19e4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: `{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-07-01T10:28:50.677Z"
}`

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 10:34:21 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-98b95e174e5054448f6781cff9951119-0bca7864ed7e4445-00","errors":{"":["A non-empty request body is required."],"id":["The value '99999999999999\n' is not valid."]}}`

## Ошибочный Запрос PUT к Books без использования тела запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/Books/1

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: c6ba747a-f695-4b2e-ab82-70ce95f30c3b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 10:39:03 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-486ea60fed54eb41b292b2591698a43a-168ebca03ef9b242-00","errors":{"":["A non-empty request body is required."]}}`

## Запрос DELETE к Books с использованием ID

URL: https://fakerestapi.azurewebsites.net/api/v1/Books/1

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 4994e61b-d20b-4bf4-abde-5e88702c54aa
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Length: 0
Date: Sat, 01 Jul 2023 10:42:39 GMT
Server: Kestrel
api-supported-versions: 1.0Content-Length: 0
Date: Sat, 01 Jul 2023 10:42:39 GMT
Server: Kestrel
api-supported-versions: 1.0`

Тело ответа: Отсутствует

## Ошибочный Запрос DELETE к Books с использованием неправильного ID
URL: https://fakerestapi.azurewebsites.net/api/v1/Books/99999999999999

Ожидаемый результат: Ответ от сервера с 400 статус кодом

Заголовки запроса:  `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.29.0
Cache-Control: no-cache
Postman-Token: 36dcc984-4c2d-4588-b731-cf6e59aed2d0
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive `

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 10:45:38 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-e3d49b023be9c546abd49222b0000822-d0f03bf5006b0043-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___


## Запрос GET к CoverPhotos
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos"

Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 634fa42a-09d0-4c98-b2c5-459173a2d211
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 11:28:10 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
``{"id":1,"idBook":1,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"}``

## Запрос POST к CoverPhotos
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos"

Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

Заголовки запроса: Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: e4031a73-b782-4050-af6e-cea409c427b7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 51`

Тело запроса: `{
  "id": 0,
  "idBook": 0,
  "url": "string"
}`

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 12:27:59 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
`{"id":0,"idBook":0,"url":"string"}`

## Запрос POST к CoverPhotos без тела запроса
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos"

Ожидаемый результат Ответ от сервера с 400 статус кодом и отчет об ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: f43841c8-514d-4944-8714-ac5f54c6bf6a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 12:26:12 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-93deb29e5a41364ca6870164d085041a-7c31c6d2300e6b41-00","errors":{"":["A non-empty request body is required."]}}`

## Запрос GET к CoverPhotos с использованием ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{{cover_id}}"

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: b488b53c-d4b0-475e-a39c-cbad87be6c78
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 12:32:26 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
`[{"id":1,"idBook":1,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"}]`

## Запрос GET к CoverPhotos с использованием несуществующего ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/201"

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 201 ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 4f70ad4b-dc28-4db6-92e7-293645956faf
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 12:35:59 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
`[]`

## Запрос GET к CoverPhotos с использованием несуществующего ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке 

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 7d457179-2004-489c-ac74-e36ad8eb68c9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 12:40:17 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-6c4b0caaf0d94f408ca86a3355e92a22-77fb0c0e8990c748-00","errors":{"idBook":["The value '99999999999999' is not valid."]}}`

## Запрос GET к CoverPhotos с использованием ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cover_id}}"

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: f5e0ed8d-b2ab-4f9a-bf29-075d5d1934c9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 12:46:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
`{"id":1,"idBook":1,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"}`

## Запрос GET к CoverPhotos с использованием несуществующего ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/201"

Ожидаемый результат: Ответ от сервера с 404 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 074aaf18-df65-4284-a2d5-187974930aa1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 01 Jul 2023 12:49:33 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-6da96cc080e3e3478db13ba23d6d23c1-0a21c63980936540-00"}`

## Запрос GET к CoverPhotos с использованием несуществующего ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 89dca503-26a8-4465-ac50-26efedb757a8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 12:52:41 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-eb206917f0d1464c8e19341b5622750b-12e1035aa6f97a4a-00","errors":{"id":["The value '99999999999999' is not valid."]}}`

## Запрос PUT к CoverPhotos с использованием ID

URL "https://fakerestapi.azurewebsites.net/api​/v1​/CoverPhotos​/{{cover_id}}}"

Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 92ecf310-bd07-49ef-b71e-a895986e0d78
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 51`

Тело запроса: `{
  "id": 0,
  "idBook": 0,
  "url": "string"
}`

Заголовки ответа `Content-Length: 0
Date: Sat, 01 Jul 2023 13:06:18 GMT
Server: Kestrel`

Тело ответа 
`{"id":0,"idBook":0,"url":"string"}`

## Запрос PUT к CoverPhotos с использованием несуществующего ID

URL "https://fakerestapi.azurewebsites.net/api​/v1​/CoverPhotos​/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 791ae7da-f943-48af-88ab-050352ad9138
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 51`

Тело запроса: `{
  "id": 0,
  "idBook": 0,
  "url": "string"
}`

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 13:19:57 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-f93bb8575930e745a2d8399d85d7c254-bda0112cdf6d4a4b-00","errors":{"id":["The value '99999999999999' is not valid."]}}`

## Запрос PUT к CoverPhotos с без тела запроса

URL "https://fakerestapi.azurewebsites.net/api​/v1​/CoverPhotos​/1"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 127a1f12-81ea-4815-87b8-0bf984d747c4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 13:22:07 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0abe2bd844ee7b46a4aeb4a3d37e1b8b-76b9bc1d7a594946-00","errors":{"":["A non-empty request body is required."]}}`

## Запрос DELETE к CoverPhotos с использованием ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cover_id}}"

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 72d34609-3b78-4577-ae0b-c32522a44d8f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Length: 0
Date: Sat, 01 Jul 2023 13:29:11 GMT
Server: Kestrel`

Тело ответа: отсутствует

## Запрос DELETE к CoverPhotos cиспользованием несуществующего ID
URL "https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: df53c302-87cc-41b3-b381-1ebc22e22474
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа `Content-Type: application/problem+json; charset=utf-8
Date: Sat, 01 Jul 2023 13:32:29 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b8ab7efea89f8e4aa0f318c11769241f-8b7bfcbff4165546-00","errors":{"id":["The value '99999999999999' is not valid."]}}`

## Запрос GET к Users


URL  "https://fakerestapi.azurewebsites.net/api/v1/Users"

Ожидаемый результат Ответ от сервера с 200 статус кодом и списком ID

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: cbff4cdf-9d8b-4dc2-9b56-c0c37de39026
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: 
`Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 16:56:58 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа :
`{"id":1,"userName":"User 1","password":"Password1"},{"id":2,"userName":"User 2","password":"Password2"},{"id":3,"userName":"User 3","password":"Password3"},{"id":4,"userName":"User 4","password":"Password4"},{"id":5,"userName":"User 5","password":"Password5"},{"id":6,"userName":"User 6","password":"Password6"},{"id":7,"userName":"User 7","password":"Password7"},{"id":8,"userName":"User 8","password":"Password8"},{"id":9,"userName":"User 9","password":"Password9"},{"id":10,"userName":"User 10","password":"Password10"}`


## Запрос POST к Users

URL "https://fakerestapi.azurewebsites.net/api/v1/Users"

Ожидаемый результат Ответ от сервера с 200 статус кодом и запись ID в список

Заголовки запроса: `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 79aa2d81-529c-4355-b36d-07fdd304e362
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 65`

Тело запроса:
`{
  "id": 0,
  "userName": "string",
  "password": "string"
}`

Заголовки ответа:  
`Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 17:51:41 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа:
`{"id":0,"userName":"string","password":"string"}`



## Запрос POST к Users без тела

URL "https://fakerestapi.azurewebsites.net/api/v1/Users"

Ожидаемый результат:  Ответ от сервера с 400 статус кодом и отчет об ошибке

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: b7c29f91-a330-4e4c-8e30-77a7f5b5ee0b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: Отсутствует

Заголовки ответа :
`Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 18:04:20 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа:
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b950cf194262904eb901b40955672243-f178722fa0ecf042-00","errors":{"":["A non-empty request body is required."]}}`


## Запрос GET к Users с использованием ID

URL "https://fakerestapi.azurewebsites.net/api/v1/Users/1"

Ожидаемый результат: Ответ от сервера с 200 статус кодом и информацией по 1 ID

Заголовки запроса: `User-Agent: PostmanRuntime/7.32.3
Accept: */*
Postman-Token: cc1fda51-24eb-4712-a706-30e0580d376c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: `Content-Type: application/json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 18:25:58 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: 
`{"id":1,"userName":"User 1","password":"Password1"}`


## Ошибочный Запрос GET к Users с использованием несуществующего ID

URL "https://fakerestapi.azurewebsites.net/api/v1/Users/11"

Ожидаемый результат: Ответ от сервера с 404 статус кодом и информацией по ошибке

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: b32e44fc-7462-4d04-a0c9-800aacb16ec1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: 
`Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Fri, 30 Jun 2023 18:42:11 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0`

Тело ответа: 

`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-246d311916683a41a417c4821f0da08c-cc947329cc829049-00"}`


## Ошибочный Запрос GET к Users с использованием несуществующего ID


URL "https://fakerestapi.azurewebsites.net/api/v1/Users/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 510e42f7-a5d8-4833-95dc-bb9d3299a4ef
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: 
`Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 18:50:15 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-4c86d7f7697fa942adeff8e781cf14fb-53352ea6de98694b-00","errors":{"id":["The value '99999999999999' is not valid."]}}`



## Запрос PUT к Users с использованием ID

URL "https://fakerestapi.azurewebsites.net/api/v1/Users/1"

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса:
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 1e88e986-7ffd-47d0-8f31-a95f3ec1a9be
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 66`

Тело запроса: 
`{
  "id": 0,
  "userName": "string",
  "password": "string"
}`

Заголовки ответа: 
`Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 19:06:30 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-56ec8a9c476e8340bde5aa239065ac57-7b26f09197de1444-00","errors":{"$":["'' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 0 | BytePositionInLine: 1."]}}`


## Ошибочный Запрос PUT к Users с использованием неправильного ID

URL: "https://fakerestapi.azurewebsites.net/api/v1/Users/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: 
`Content-Type: application/json Accept: text/plain User-Agent: PostmanRuntime/7.32.3 Postman-Token: a18daa01-cfe8-4143-abdc-195b2528f22d Host: fakerestapi.azurewebsites.net Accept-Encoding: gzip, deflate, br Connection: keep-alive Content-Length: 101Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: 0404913a-96b1-425a-afc2-beeedd21faed
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 65`


Тело запроса:

 `{
  "id": 0,
  "userName": "string",
  "password": "string"
}`


Заголовки ответа: 
`Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 19:17:50 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-bdf5cc0283e8fb4d98a7e0e0d2d3b42d-77a676332b357543-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___
## Ошибочный Запрос PUT к Users без использования тела запроса


URL: "https://fakerestapi.azurewebsites.net/api/v1/Users/1"

Ожидаемый результат: Ответ от сервера с 400 статус кодом и информацией по ошибке

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: c29b7331-d358-4977-a51f-3c566e6a918e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0`

Тело запроса: Отсутствует

Заголовки ответа: 
`Content-Type: application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 19:27:24 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа:
 `{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-768cced56496044d9d158c4d47fac093-b8d49a08a7619f41-00","errors":{"":["A non-empty request body is required."]}}`
___
## Запрос DELETE к Users с использованием ID

URL: "https://fakerestapi.azurewebsites.net/api/v1/Users/1"

Ожидаемый результат: Ответ от сервера с 200 статус кодом

Заголовки запроса:
 `Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: e7478248-2040-4ca4-a0d2-82680d4a64df
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: 
`Content-Length: 0
Date: Fri, 30 Jun 2023 19:40:28 GMT
Server: Kestrel
api-supported-versions: 1.0`

Тело ответа: Отсутствует
___

## Ошибочный Запрос DELETE к Users с использованием неправильного ID

URL: "https://fakerestapi.azurewebsites.net/api/v1/Users/99999999999999"

Ожидаемый результат: Ответ от сервера с 400 статус кодом

Заголовки запроса: 
`Content-Type: application/json
Accept: text/plain
User-Agent: PostmanRuntime/7.32.3
Postman-Token: abb308db-5da6-418f-b48f-a582f4987f1c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive`

Тело запроса: Отсутствует

Заголовки ответа: 
Content-Type: `application/problem+json; charset=utf-8
Date: Fri, 30 Jun 2023 19:45:46 GMT
Server: Kestrel
Transfer-Encoding: chunked`

Тело ответа: 
`{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-dda0c55a21ebb441a8a774c622aa8303-89bfe619f1c1d941-00","errors":{"id":["The value '99999999999999' is not valid."]}}`
___