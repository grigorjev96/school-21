# Горячие клавиши DevTools
## Keyboard shortcuts for opening DevTools
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Action	|Mac	|Windows / Linux|
|Open whatever panel you used last|	Command+Option+I|	F12 or Control+Shift+I|
|Open the Console panel|	Command+Option+J|	Control+Shift+J
|Open the Elements panel	|Command+Shift+C or Command+Option+C|	Control+Shift+C|
|<!-- -->|<!-- -->|<!-- -->|
## Global keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Show Settings|	? or Function+F1|	? or F1|
|Focus the next panel|	Command+] |	Control+] |
|Focus the previous panel|	Command+[|	Control+[|
|Switch back to whatever docking position you last used. If DevTools has been in its default position for the entire session, then this shortcut undocks DevTools into a separate window|	Command+Shift+D|	Control+Shift+D|
|Toggle Device Mode|	Command+Shift+M|	Control+Shift+M|
|Toggle Inspect Element Mode|	Command+Shift+C|	Control+Shift+C|
|Open the Command Menu|	Command+Shift+P|	Control+Shift+P|
|Toggle the Drawer|	Escape|	Escape|
|Normal reload|	Command+R|	F5 or Control+R|
|Hard reload|	Command+Shift+R|	Control+F5 or Control+Shift+R|
Search for text within the current panel. Supported only in the Elements, Console, Sources, Performance, Memory, JavaScript Profiler, and Quick Source panels.|	Command+F|	Control+F|
|Opens the Search tab in the Drawer, which lets you search for text across all loaded resources|	Command+Option+F|	Control+Shift+F|
|Open a file in the Sources panel|	Command+O or Command+P|	Control+O or Control+P|
|Zoom in|	Command+Shift++|	Control+Shift++|
|Zoom out|	Command+-|	Control+-|
|Restore default zoom level|	Command+0|	Control+0|
|Run snippet|	Press Command+O to open the Command Menu, type ! followed by the name of the script, then press Enter|	Press Control+O to open the Command Menu, type ! followed by the name of the script, then press Enter|
## Elements panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Undo change|	Command+Z|	Control+Z|
|Redo change|	Command+Shift+Z|	Control+Y|
|Select the element above / below the currently-selected element|	Up Arrow / Down Arrow|	Up Arrow / Down Arrow
|Expand the currently-selected node. If the node is already expanded, this shortcut selects the element below it|	Right Arrow	|Right Arrow
|Collapse the currently-selected node. If the node is already collapsed, this shortcut selects the element above it|	Left Arrow|	Left Arrow
|Expand or collapse the currently-selected node and all of its children|	Hold Option then click the arrow icon next to the element's name|	Hold Control+Alt then click the arrow icon next to the element's name|
|Toggle Edit Attributes mode on the currently-selected element|	Enter|	Enter
|Select the next / previous attribute after entering Edit Attributes mode|	Tab / Shift+Tab|	Tab / Shift+Tab
|Hide the currently-selected element|	H|	H|
|Toggle Edit as HTML mode on the currently-selected element|	Function+F2	|F2
## Styles pane keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Go to the line where a property value is declared|	Hold Command then click the property value|	Hold Control then click the property value
|Cycle through the RGBA, HSLA, and Hex representations of a color value|	Hold Shift then click the Color Preview box next to the value|	Hold Shift then click the Color Preview box next to the value
|Select the next / previous property or value|	Click a property name or value then press Tab / Shift+Tab|	Click a property name or value then press Tab / Shift+Tab
|Increment / decrement a property value by 0.1|	Click a value then press Option+Up Arrow / Option+Down Arrow|	Click a value then press Alt+Up Arrow / Alt+Down Arrow
|Increment / decrement a property value by 1|	Click a value then press Up Arrow / Down Arrow|	Click a value then press Up Arrow / Down Arrow
|Increment / decrement a property value by 10|	Click a value then press Shift+Up Arrow / Shift+Down Arrow|	Click a value then press Shift+Up Arrow / Shift+Down Arrow
|Increment / decrement a property value by 100|	Click a value then press Command+Up Arrow / Command+Down Arrow|	Click a value then press Control+Up Arrow / Control+Down Arrow
|Cycle through the degrees (deg), gradians (grad), radians (rad) and turns (turn) representations of an angle value	|Hold Shift then click the Angle Preview box next to the value|	Hold Shift then click the Angle Preview box next to the value
|Increment / decrement an angle value by 1	|Click the Angle Preview box next to the value then press Up Arrow / Down Arrow	|Click the Angle Preview box next to the value then press Up Arrow / Down Arrow
|Increment / decrement an angle value by 10	|Click the Angle Preview box next to the value then press Shift+Up Arrow / Shift+Down Arrow	|Click the Angle Preview box next to the value then press Shift+Up Arrow / Shift+Down Arrow
|Increment / decrement an angle value by 15|	Click the Angle Preview box next to the value then press Shift, click / mouse slide on the Angle Clock Overlay|	Click the Angle Preview box next to the value then press Shift, click / mouse slide on the Angle Clock Overlay
## Sources panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Pause script execution (if currently running) or resume (if currently paused)|	F8 or Command+\	|F8 or Control+\
|Step over next function call|	F10 or Command+'|	F10 or Control+'
|Step into next function call|	F11 or Command+;	|F11 or Control+;
|Step out of current function|	Shift+F11 or Command+Shift+;|	Shift+F11 or Control+Shift+;
|Continue to a certain line of code while paused|	Hold Command and then click the line of code|	Hold Control and then click the line of code|
|Select the call frame below / above the currently-selected frame|	Control+. / Control+,|	Control+. / Control+,|
|Save changes to local modifications|	Command+S|	Control+S|
|Save all changes|	Command+Option+S|	Control+Alt+S|
|Go to line|	Control+G	|Control+G|
|Jump to a line number of the currently-open file	|Press Command+O to open the Command Menu, type : followed by the line number, then press Enter|	Press Control+O to open the Command Menu, type : followed the line number, then press Enter|
|Jump to a column of the currently-open file (for example line 5, column 9)|	Press Command+O to open the Command Menu, type :, then the line number, then another :, then the column number, then press Enter|	Press Control+O to open the Command Menu, type :, then the line number, then another :, then the column number, then press Enter
|Go to a function declaration (if currently-open file is HTML or a script), or a rule set (if currently-open file is a stylesheet)	|Press Command+Shift+O, then type in the name of the declaration / rule set, or select it from the list of options|	Press Control+Shift+O, then type in the name of the declaration / rule set, or select it from the list of options
|Close the active tab|	Option+W|	Alt+W
|Open next or previous tab|	Function+Command+Up or Down	Control+Page| Up or Page Down
|Toggle the Navigation sidebar on the left|	Command+Shift+Y	|Control+Shift+Y
|Toggle the Debugger sidebar on the right|	Command+Shift+H|	Control+Shift+H
## Code Editor keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Delete all characters in the last word, up to the cursor	|Option+Delete|	Control+Delete
|Add or remove a line-of-code breakpoint	|Focus your cursor on the line and then press Command+B	|Focus your cursor on the line and then press Control+B
|Open the breakpoint edit dialog to edit conditional breakpoints or logpoints|	Focus your cursor on the line and then press Command+Alt+B	|Focus your cursor on the line and then press Control+Alt+B
|Go to matching bracket|	Control+M|	Control+M
|Toggle single-line comment. If multiple lines are selected, DevTools adds a comment to the start of each line|	Command+/	|Control+/
|Select / de-select the next occurrence of whatever word the cursor is on. Each occurrence is highlighted simultaneously|	Command+D / Command+U|	Control+D / Control+U|
## Network panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Start / stop recording	|Command+E|	Control+E
Record a reload	|Command+R	|Control+R
Replay a selected XHR request	|R|	R
Hide the details of a selected request|	Escape|	Escape|
## Performance panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Start / stop recording|	Command+E|	Control+E
|Save recording	|Command+S	|Control+S
|Load recording	|Command+O	|Control+O
## Memory panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Start / stop recording|	Command+E|	Control+E|
## Console panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Accept autocomplete suggestion|	Right Arrow or Tab|	Right Arrow or Tab|
|Reject autocomplete suggestion|	Escape|	Escape|
|Navigate the autocomplete list up or down	|Up / Down or Control+P / N|	Up / Down or Control+P / N
|Get previous statement	|Up Arrow|	Up Arrow|
|Get next statement	|Down Arrow|	Down Arrow
|Focus the Console|	Control+`	|Control+`|
|Clear the Console|	Command+K or Option+L	|Control+L
|Force a multi-line entry. Note that DevTools should detect multi-line scenarios by default, so this shortcut is now usually unnecessary|	Shift+Return|	Shift+Enter|
|Execute|	Return|	Enter|
|Expand all sub-properties of an object that's been logged to the Console|	Hold Alt then click Expand >|	Hold Alt then click Expand >|
## Search tab keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
Expand/collapse all search results|	Command+Option+{ or }|	Control+Shift+{ or }|
## Recorder panel keyboard shortcuts
|<!-- -->|<!-- -->|<!-- -->|
|:-----|:---------:|-----------:|
|Start or stop recording|	Command+E|	Control+E
|Replay recording|	Command+Enter|	Control+Enter
|Copy recording or selected step|	Command+C|	Control+C
|Toggle code view|	Command+B|	Control+B|
# Описание DevTools
**Chrome DevTools** - это набор инструментов разработчика, встроенных в браузер Google Chrome, предназначенных для анализа, отладки и профилирования веб-приложений. Они предоставляют разработчикам широкий спектр возможностей для исследования и оптимизации процесса разработки веб-сайтов и веб-приложений. DevTools доступны в браузере Chrome и позволяют разработчикам получить полный контроль над кодом, стилями и производительностью своих веб-приложений.

Вкладки DevTools содержат различные инструменты и панели для разных задач. Ниже приведено описание некоторых основных вкладок DevTools:

1. Elements (Элементы): Эта вкладка позволяет анализировать и изменять HTML и CSS код веб-страницы в режиме реального времени. Вы можете просматривать и редактировать DOM-структуру, применять стили, удалять или изменять элементы на странице и многое другое.

2. Console (Консоль): Эта вкладка используется для вывода сообщений JavaScript и выполнения команд в контексте текущей страницы. Здесь вы можете проверять и отлаживать код JavaScript, а также использовать консоль для выполнения различных задач и экспериментов.

3. Sources (Исходники): В этой вкладке можно просматривать, редактировать и отлаживать исходный код JavaScript, CSS и других ресурсов веб-страницы. Вы можете устанавливать точки останова, отслеживать выполнение кода, анализировать стек вызовов и многое другое.

4. Network (Сеть): Здесь отображается информация о запросах и ответах, связанных с текущей веб-страницей. Вы можете анализировать время загрузки ресурсов, отслеживать запросы, проверять заголовки и тела ответов, а также имитировать различные условия сети для тестирования производительности.

5. Performance (Производительность): В этой вкладке можно профилировать производительность веб-приложений. Вы можете анализировать времена выполнения, идентифицировать узкие места и оптимизировать код для повышения производительности приложения.

6. Application (Приложение): Здесь вы можете анализировать хранилище данных, такое как cookies, localStorage, IndexedDB и другие. Вы можете просматривать, редактировать и удалять данные, а также симулировать различные условия хранения данных для тестирования приложения.

7. Security (Безопасность): В этой вкладке можно анализировать безопасность веб-сайта или веб-приложения. Вы можете проверить сертификаты SSL/TLS, анализировать безопасность соединения и идентифицировать потенциальные уязвимости.

8. Lighthouse - это инструмент анализа качества веб-страниц и веб-приложений, интегрированный в Chrome DevTools. Он предоставляет разработчикам подробный отчет о производительности, доступности, процессе загрузки и других аспектах веб-сайта. Lighthouse автоматически выполняет ряд анализов и аудитов, основанных на лучших практиках разработки, включая рекомендации от Google по оптимизации производительности и доступности. Результаты анализа представлены в виде отчета, который включает метрики производительности, рекомендации по оптимизации, проблемы безопасности и другую полезную информацию.