## Баг-репорт №1
1. Заголовок:

`Отображение статус-кода 404 Not Found в консоли DevTools после нажатия на иконку "Лента-Сбермаркет" на главной странице
https://sbermegamarket.ru`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru

3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на иконку "Лента-Сбермаркет"
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools
- проверить заголовки ответа и заголовки запроса

5. Ожидаемый результат:

отсутствие ошибки с кодом 404 Not Found

6. Фактический результат:

`отображение кода 404 Not Found в запросе POST /https://stream.datago.ru/mp/collect?tid=UA-89387429-1 в консоли DevTools
отображение следующих заголовков ответа:`

    content-length: 153
    content-type: text/html
    date: Mon, 26 Jun 2023 12:14:01 GMT
    server: ycalb
    X-Firefox-Spdy: h2

отображение следующих заголовоков запроса:

    Accept: */*
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
    Connection: keep-alive
    Content-Length: 1070
    Content-Type: text/plain;charset=UTF-8
    Host: stream.datago.ru
    Origin: https://sbermegamarket.ru
    Referer: https://sbermegamarket.ru/
    Sec-Fetch-Dest: empty
    Sec-Fetch-Mode: no-cors
    Sec-Fetch-Site: cross-site
    TE: trailers
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0         
7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №2
1. Заголовок:

`Отображение статус-кода 400 Bad Request в консоли DevTools после нажатия на кнопку "Марафон выгоды" на главной странице
https://sbermegamarket.ru`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru


3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на кнопку "Марафон выгоды"
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools
- проверить заголовки ответа и заголовки запроса

5. Ожидаемый результат:

отсутствие ошибки с кодом 400 Bad Request

6. Фактический результат:


`отображение кода 400 Bad Request в запросе POST /https://sbermegamarket.ru/api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm
=TQ00eV/44LD4MuNsY399ZlYpWR0aG9FDCzc69EMhEyyMe30f+mHzaUg63RlFKQHaU5ZPrn1a+n3N50i0V34t97p6jrdNZfPTQh6BAet5KsQ+N2hfDG9F8tla
ZFn0SbWkJU6zmx724GIiVYlTz1BS/aLlgJ2dk8IpCu5rcyE= в консоли DevTools`


Oтображение следующих заголовков ответа:


    access-control-allow-credentials: true
    access-control-allow-headers:Accept,DNT,Keep-Alive,User-Agent,If-Modified-Since,Cache-Control,Content-Type,Origin,ETag,If-None-Match,X-Cfids,Authorization
    access-control-allow-methods: GET, POST, OPTIONS
    access-control-allow-origin: https://sbermegamarket.ru
    content-length: 0
    date: Mon, 26 Jun 2023 14:15:33 GMT
    server: nginx
    strict-transport-security: max-age=31536000; includeSubDomains
    x-envoy-upstream-service-time: 2
    X-Firefox-Spdy: h2
    x-sp-crid: 983623176:1

отображение следующих заголовоков запроса:

    Accept: */*
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
    Connection: keep-alive
    Content-Length: 13024
    Content-Type: text/plain;charset=UTF-8
    Cookie: spid=1687714298920_deadd6ee2047b4b32de1243c6c1ac05c_wpnoebnqptf35mjw;
    _ym_uid=1687714306535323669; _ym_d=1687714306; device_id=2978af86-137e-11ee-bd76-0242ac110003;
    sbermegamarket_token=512a8dd7-06bb-4fdb-9de0-b01cc7b95c22; adspire_uid=AS.863160776.1687714311;
    ssaid=2bb84e10-137e-11ee-864e-47c3eddf57b7; cfidsw-smm=TQ00eV/44LD4MuNsY399ZlYpWR0aG9FDCzc69EMhEyy
    Me30f+mHzaUg63RlFKQHaU5ZPrn1a+n3N50i0V34t97p6jrdNZfPTQh6BAet5KsQ+N2hfDG9F8tlaZFn0SbWkJU6zmx724GIiVYlTz1BS/aLlgJ2dk8IpCu5rcyE=;
    cfidsw-smm=TQ00eV/44LD4MuN… uxs_uid=350ac790-137e-11ee-8020-c9a47d310781; _gpVisits={"isFirstVisitDomain":true,"idContainer":"10002472"};
    _gp10002472={"hits":2,"vc":1,"ac":1,"a6":1}; adrcid=AQZ8x87IcpfJD2nZXPjU4pg; tmr_lvid=252d39f672b451fa2f3e843bc461086e; tmr_lvidTS=1687714328644;
    _gid=GA1.2.716832827.1687714329; flocktory-uuid=4fede27f-561d-4517-86d0-0578fb1d8215-0; tmr_detect=0%7C1687776985020; isOldUser=true;
    spsc=1687776888664_200606f03a76c1908a2a58355f575c97_a5476469b72f558bb72e6aae99c6a060; __tld__=null; rrlevt=1687778921792
    Host: sbermegamarket.ru
    Origin: https://sbermegamarket.ru
    Referer: https://sbermegamarket.ru/landing/shopping-fest/
    Sec-Fetch-Dest: empty
    Sec-Fetch-Mode: cors
    Sec-Fetch-Site: same-origin
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №3

1. Заголовок:

`Отображение статус-кода 502 Bad Gateway в консоли DevTools при загрузке раздела детской одежды на главной странице
https://sbermegamarket.ru`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru


3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на кнопку "Одежда и обувь"
- нажать на раздел "Детям"
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools
- проверить заголовки ответа и заголовки запроса

5. Ожидаемый результат:

отсутствие ошибки с кодом 502 Bad Gateway

6. Фактический результат:


`отображение кода 502 Bad Gateway в запросе GET /https://sbermegamarketru.webim.ru/button.php в консоли DevTools`

отображение следующих заголовков ответа:

    Connection: keep-alive
    content-length: 1943
    content-type: text/html
    date: Mon, 26 Jun 2023 16:00:13 GMT
    ETag: "6488db85-797"
    Server: nginx

отображение следующих заголовоков запроса:

    Accept: image/avif,image/webp,*/*
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
    Connection: keep-alive
    Host: sbermegamarketru.webim.ru
    Referer: https://sbermegamarket.ru/
    Sec-Fetch-Dest: image
    Sec-Fetch-Mode: no-cors
    Sec-Fetch-Site: cross-site
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

Баг-репорт №4

1. Заголовок:

`Отображение статус-кода 503 Service Unavailable в консоли DevTools после нажатия на круглую иконку "Телеграм"
в футере на главной странице https://sbermegamarket.ru`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru


3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на круглую иконку "Телеграм" в футере на главной странице
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools
- проверить заголовки ответа и заголовки запроса

5. Ожидаемый результат:

отсутствие ошибки с кодом 503 Service Unavailable

6. Фактический результат:


`отображение кода 503 Service Unavailable в запросе POST /https://sentry-dsn-customers.megamarket.tech/api/6/envelope/?sentry_key=6d82cc11d08a4657a9092ff80b4bd615&sentry_version=7 в консоли DevTools`


отображение следующих заголовков ответа:

    access-control-allow-origin: https://sbermegamarket.ru
    access-control-expose-headers: x-sentry-rate-limits, x-sentry-error, retry-after
    Connection: keep-alive
    Content-Length: 84
    Content-Type: application/json
    Date: Mon, 26 Jun 2023 17:41:47 GMT
    Server: nginx
    Strict-Transport-Security: max-age=31536000; includeSubDomains
    vary: Origin

отображение следующих заголовоков запроса:

    Accept: */*
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
    Connection: keep-alive
    Content-Length: 134
    Content-Type: text/plain;charset=UTF-8
    Host: sentry-dsn-customers.megamarket.tech
    Origin: https://sbermegamarket.ru
    Referer: https://sbermegamarket.ru/
    Sec-Fetch-Dest: empty
    Sec-Fetch-Mode: no-cors
    Sec-Fetch-Site: cross-site
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0         
7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №5

1. Заголовок:

`Неуспешный запрос после нажатия на круглую иконку "VK" в футере на главной странице https://sbermegamarket.ru,
ошибка Open api access error в консоли DevTools`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru

3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на круглую иконку "VK" в футере на главной странице
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools

5. Ожидаемый результат:

`отсутствие ошибки Open api access error в консоли DevTools после нажатия на иконку "VK" в футере на главной странице сайта, успешный запрос`

6. Фактический результат:


отображение ошибки Open api access error в консоли DevTools,


неуспешный запрос:


 `Запрос из постороннего источника заблокирован: 
Политика одного источника запрещает чтение удаленного ресурса на
«https://vk.com/rtrg?p=VK-RTRG-1042270-gnJEy&products_event=view_category&price_list_id=343773&e=1&i=0&metatag_url=%2F%2Fsbermegamarket.ru
%2Fcatalog%2Farabika%2Fpage-2%2F&metatag_title=%D0%9A%D0%BE%D1%84%D0%B5%20%22%D0%90%D1%80%D0%B0%D0%B1%D0%B8%D0%BA%D0%B0%22%20-%20%D0%BC%D0%B0
%D1%80%D0%BA%D0%B5%D1%82%D0%BF%D0%BB%D0%B5%D0%B9%D1%81%20sbermegamarket.ru&products_params=%7B%22category_ids%22%3A%5B%22272416%22%5D%2C%22products_
recommended_ids%22%3A%5B%22600009188373%22%2C%22600005313493%22%2C%22600008811324%22%2C%22100048026000%22%5D%7D».
(Причина: Учётные данные не поддерживаются, если заголовок CORS «Access-Control-Allow-Origin» установлен в «*»)`

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №6

1. Заголовок:

`Ошибка Uncaught (in promise) Error: Blocked request в консоли DevTools, возникающая
при скроллинге товаров в вертикальном положении в разделе "Мелкая техника для кухни" в Каталоге товаров

2. Предшествующие условия:`

открыта главная страница https://sbermegamarket.ru

3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- проскроллить товары в разделе "Мелкая техника для кухни" в Каталоге товаров на главной странице
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools

5. Ожидаемый результат:

отсутствие ошибки Uncaught (in promise) Error: Blocked request в консоли DevTools

6. Фактический результат:

отображение ошибки Uncaught (in promise) Error: Blocked request в консоли DevTools

    browserHttpRequest https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    request https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    post https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    fetch https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    fetch https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    s https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    setTimeout handler*l/< https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    fetch https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    fetch https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    fetchUrlParams https://extra-cdn.sbermegamarket.ru/static/dist/services.391ca3b6.js:1
    r https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    72002 https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    r https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    Ce https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    confirmTransition https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    transitionTo https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    push https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    push https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    push https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    onBackButtonClick https://extra-cdn.sbermegamarket.ru/static/dist/catalog.ad58d351.js:1
    click https://extra-cdn.sbermegamarket.ru/static/dist/catalog.ad58d351.js:1
    nn https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    n https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _wrapper https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    s https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    p https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    To https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    ht https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    Lo https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    m https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    xi https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _update https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    r https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    get https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    e https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    mount https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    $mount https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    init https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    k https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    k https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    xi https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _update https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    r https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    get https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    run https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №7

1. Заголовок:

`Отображение статус-кода 404 Not Found в консоли DevTools после нажатия на гиперссылку "О компании" в разделе "Маркетплэйс"
в футере на главной странице сайта https://sbermegamarket.ru`

2. Предшествующие условия:

открыта главная страница https://sbermegamarket.ru


3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на гиперссылку "О компании" в разделе "Маркетплейс" в футере на главной странице
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools
- проверить заголовки ответа и заголовки запроса

5. Ожидаемый результат:

отсутствие ошибки с кодом 404 Not Found

6. Фактический результат:


отображение кода 404 Not Found в консоли DevTools в запросе GET /https://sbermegamarket.ru/info/about-sbermegamarket-ru/mobile/css/mobile.css


отображение следующих заголовков ответа:


    content-encoding: gzip
    content-type: text/html
    date: Tue, 27 Jun 2023 19:11:16 GMT
    server: nginx
    strict-transport-security: max-age=31536000; includeSubDomains
    X-Firefox-Spdy: h2
    x-sp-crid: 1252214859:63

отображение следующих заголовоков запроса:

    Accept: text/css,*/*;q=0.1
    Accept-Encoding: gzip, deflate, br
    Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
    Connection: keep-alive
    Cookie spid=1687714298920_deadd6ee2047b4b32de1243c6c1ac05c_wpnoebnqptf35mjw;
    _ym_uid=1687714306535323669; _ym_d=1687714306; device_id=2978af86-137e-11ee-bd76-0242ac110003;
    sbermegamarket_token=512a8dd7-06bb-4fdb-9de0-b01cc7b95c22; adspire_uid=AS.863160776.1687714311;
    ssaid=2bb84e10-137e-11ee-864e-47c3eddf57b7; cfidsw-smm=ytZKrnjOXwIYxnK2S1fQReXfj7Pdfm1Gzj4QY3l
    JveWvXoet8cfkeWrUCilKj1CHr4UYXC66W4DOmzgz14abVuPWTBj3ON2ih/FFCnrCOAXY8uQwodQLLtjVnZ+xo2YJQHLqn1q/iFGxkfxGqW+/uHCqp8I6QigQnUpnQag=;
    cfidsw-smm=ytZKrnjOXwIYxnK…sFirstVisitDomain":true,"idContainer":"10002472"}; _gp10002472={"hits":12,"vc":1,"ac":1,"a6":1};
    adrcid=AQZ8x87IcpfJD2nZXPjU4pg; tmr_lvid=252d39f672b451fa2f3e843bc461086e; tmr_lvidTS=1687714328644; _gid=GA1.2.716832827.1687714329;
    flocktory-uuid=4fede27f-561d-4517-86d0-0578fb1d8215-0; tmr_detect=0%7C1687889601861; isOldUser=true; rrlevt=1687802655085; _ym_isad=2;
    spsc=1687892992572_da557a9093591ba173f65a9a7e448938_a5476469b72f558bb72e6aae99c6a060; _ym_visorc=b; __tld__=null; rr-testCookie=testvalue; _gat=1
    Host: sbermegamarket.ru
    Referer: https://sbermegamarket.ru/info/about-sbermegamarket-ru/
    Sec-Fetch-Dest: style
    Sec-Fetch-Mode: no-cors
    Sec-Fetch-Site: same-origin
    TE:trailers
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).

## Баг-репорт №8

1. Заголовок:

`Отображение ошибки Uncaught ReferenceError: product is not defined в консоли DevTools после добавления товара "Кофе" в Корзину из каталога товаров`

2. Предшествующие условия:

- открыта главная страница https://sbermegamarket.ru
- открыт каталог товаров
- установлен фильтр по названию "Кофе"

3. Серьёзность:

Critical

4. Шаги для воспроизведения:

- нажать на кнопку купить под товаром "Кофе EGOISTE Noir в зернаx 1000г."
- открыть инструменты разработчика DevTools - нажать клавишу F12 на клавиатуре
- проверить наличие ошибки в консоли DevTools

5. Ожидаемый результат:

`отсутствие ошибки Uncaught ReferenceError: product is not defined`

6. Фактический результат:

отображение ошибки Uncaught ReferenceError: product is not defined в консоли DevTools
данные ошибки:

    <anonymous> https://sbermegamarket.ru/catalog/kofe/#?related_search=кофе line 308 > injectedScript:1
    a https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:308
    b https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:309
    Qc https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:65
    e https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:211
    Ja https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:42
    sr https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:212
    qr https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:212
    Pr https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:213
    Xr https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:216
    et https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:234
    ht https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:239
    push https://www.googletagmanager.com/gtm.js?id=GTM-WSJ2VMC:243
    push https://www.googletagmanager.com/gtag/js?id=DC-10810778&l=dataLayer&cx=c:262
    push https://www.googletagmanager.com/gtag/js:230
    push https://top-fwz1.mail.ru/js/code.js:32
    trackEvent https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    pushEventQueue https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    o https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    s https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    s https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    fireEvent https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    fireEvent https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    push https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    u https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    fireAddRemoveProduct https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    listenToEvents https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    fireEvent https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    fireEvent https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    push https://cdn.ddmanager.ru/ddm-initialization/f72a2e9d-633c-4ab0-9cff-2c32b4d5cad6.js:1
    W https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    J https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    updateItems https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    Ne https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    ze https://extra-cdn.sbermegamarket.ru/static/dist/main.e882a233.js:1
    nn https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    n https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _wrapper https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    s https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    p https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    To https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    ht https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    Lo https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    m https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    xi https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _update https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    r https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    get https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    e https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    mount https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    $mount https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    init https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    d https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    xi https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    _update https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2
    r https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.22ca0abc.js:2

7. Окружение:

Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Firefox/114.0.2 (64-разрядный).