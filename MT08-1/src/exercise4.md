**OC Windows**

Для открытия журнала событий Windows нужно

- Нажать клавишу Win+R: Это откроет "Окно выполнения".

- Ввести "eventvwr" и нажать Enter: Это запустит приложение "Просмотр событий".
___
- Журнал "Система": В журнале "Система" регистрируются события, связанные с работой операционной системы и компьютерного оборудования. Этот журнал содержит информацию о загрузке системы, драйверах устройств, обнаруженных ошибках, сбоях и других событиях, связанных с работой ядра операционной системы.

- Журнал "Приложения": В журнале "Приложения" регистрируются события, связанные с работой установленных на компьютере приложений. Здесь могут быть записаны сообщения об ошибках, предупреждения, информационные события, связанные с выполнением приложений. Примерами таких событий могут быть ошибки при запуске программы, успешное завершение задачи или информация о событиях, связанных с базами данных.

- Журнал "Безопасность": Журнал "Безопасность" содержит информацию о событиях, связанных с безопасностью компьютера и сети. Здесь регистрируются входы в систему, события аудита, изменения настроек безопасности, неудачные попытки входа, атаки и другие безопасностные события. Этот журнал часто используется для отслеживания потенциальных угроз и анализа безопасности системы.