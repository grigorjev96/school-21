## Описание баз данных
### 1. Иерархические базы данных:

Иерархические базы данных организованы в виде древовидной структуры, где данные представлены в виде родительских и дочерних элементов или узлов. Каждый узел может иметь только одного родителя, но может иметь несколько дочерних элементов. Этот тип базы данных был одним из первых и широко использовался в ранних информационных системах. Примером иерархической базы данных является IMS (Information Management System) от IBM.
- **Особенности:** организованы в виде древовидной структуры, узлы имеют родительские и дочерние элементы.
- **Преимущества:**
  - Простота иерархической модели, легкость понимания и использования.
  - Высокая производительность при работе с простыми иерархическими структурами данных.
- **Недостатки:**
  - Ограничения на структуру данных, сложно обрабатывать связи между элементами на разных уровнях.
  - Ограниченная гибкость при изменении структуры данных.
- **Примеры:** IMS (Information Management System) от IBM, RDM (Record Definition Method).

### 2. Сетевые базы данных:

Сетевые базы данных похожи на иерархические базы данных, но с более гибкой структурой. В сетевых базах данных данные организованы в виде графа, где каждый узел может иметь несколько родителей и несколько дочерних элементов. Этот тип базы данных разработан для более сложных и гибких приложений. CODASYL DBTG (Conference on Data Systems Languages Database Task Group) является одним из наиболее известных стандартов, связанных с сетевыми базами данных.
- **Особенности:** данные организованы в виде графа, узлы могут иметь несколько родителей и дочерних элементов.
- **Преимущества:**
  - Гибкость при работе с более сложными структурами данных и связями между ними.
  - Возможность эффективно представлять и обрабатывать сложные отношения и множество-ко-множеству связи.
- **Недостатки:**
  - Сложность в понимании и использовании по сравнению с иерархическими и реляционными базами данных.
  - Ограниченная поддержка стандартов и распространение.
- **Примеры:** Integrated Data Store (IDS), IDMS (Integrated Database Management System).

### 3. Реляционные базы данных:

Реляционные базы данных основаны на теории реляционных моделей данных, предложенной Эдгаром Коддом в 1970 году. В реляционной базе данных данные организованы в виде таблиц, состоящих из строк и столбцов. Каждая таблица представляет отдельную сущность, а отношения между таблицами устанавливаются с помощью ключей. Реляционные базы данных являются наиболее распространенным типом баз данных и широко применяются во многих областях, включая бизнес и научные приложения. Примеры реляционных баз данных включают Oracle, MySQL, Microsoft SQL Server и PostgreSQL.
- **Особенности:** данные организованы в виде таблиц, отношения между таблицами устанавливаются с помощью ключей.
- **Преимущества:**
  - Простота в использовании, широкая поддержка и распространение.
  - Гибкость при изменении структуры данных и запросов.
  - Хорошая производительность при правильном использовании индексов и оптимизации запросов.
- **Недостатки:**
  - Некоторые ограничения, особенно в отношении работы с иерархическими или множественными связями.
  - Сложности в представлении сложных отношений и связей.
- **Примеры:** Oracle Database, MySQL, Microsoft SQL Server, PostgreSQL.



## Нереляционные базы данных:

**Нереляционная база данных** — это база данных, в которой в отличие от большинства традиционных систем баз данных, **не используется табличная схема строк и столбцов**. В этих базах данных **применяется модель хранения, оптимизированная под конкретные требования типа хранимых данных**. Например, данные могут храниться как простые пары "ключ — значение", документы JSON или граф, состоящий из ребер и вершин.

### Отличия от SQL:

1. Термин NoSQL применяется к хранилищам данных, которые не используют язык запросов SQL. Вместо этого они запрашивают данные с помощью других языков программирования и конструкций. На практике NoSQL означает "нереляционная база данных", даже несмотря на то, что многие из этих баз данных под держивают запросы, совместимые с SQL. 

2. СУБД без SQL позволяют записывать и получать неструктурированную информацию, дают возможность менять схемы и запросы в соответствии с требованиями к данным. Когда речь идёт о работе с time-series data, информацией без взаимосвязи, о хранении документов с разной структурой и высокой степенью вложенности, то именно NoSQL показывают большую скорость и производительность.

3. При разработке NoSQL были смягчены жесткие требования к транзакциям. Основные принципы ACID гарантируют, что целостность и согласованность данных в реляционных хранилищах будет обеспечена даже при сбоях. 

### Виды NoSQL:


**Хранилище пар "ключ — значение"**.
Данные хранятся в виде таблицы, прдставляются собой ключ и объект, который нужно к нему прикрепить. Такие базы часто используют в совокупности с другими Бд как механизм кеширования.

Большинство хранилищ пар "ключ — значение" поддерживают только самые простые операции запроса, вставки и удаления. Чтобы частично или полностью изменить значение, приложение всегда перезаписывает существующее значение целиком. В большинстве реализаций атомарной операцией считается чтение или запись одного значения. Запись больших значений занимает относительно долгое время.

**Плюсы:**
1. Можно использовать с разными типами данных(число, буква,файл) под разными ключами.

2. Адресное хранение позволет получить доступ к данным максимально быстро.

3. Шардирование по определенным ключам.

**Минусы:**
1. Отсуствуют стандартные взможности БД - атомарность транзакций, согласованность данных при одновременном параллельном исполнении.

2. Тяжело поддерживает уникальные ключи при большом объеме данных.

**Примеры:**
1. Хранилище сессий. Например, интернет-магазин. Запускается при входе. Хранит сессию пока клиент не выйдет или не истевет вермя сессии.
2. Amazon DynamoDB, Riak, Memcached.


**Хранилище данных документов** управляет набором значений именованных строковых полей и данных объекта в сущности, которая называется документом. Данные хранятся в стуктуированных форматах XML, YAML, JSON, BSON, или хранить в виде обычного текста. Каждое значение поля может представлять собой скалярный элемент, например число, или сложный объект, например список или коллекция типа "родитель — потомок". Поля документов доступны системе управления хранилищем, что позволяет приложению выполнять запросы и применять фильтры, основанные на значениях этих полей.

**Плюсы:**
1. Подходит для сервисов с разной структурой данных.

2. Легко меняет стурктуру и масштаб.

3. позволяет быстро создать документ и  требует малых затрат на обслуживание.

4. все документы не зависимы друг от друга, потому что не иимеют ключей.

5. Для описания документов использует простые языки программирования.

**Минусы:**
1. ограничения по проверке на согласованность, что снижает производительность.
2. Сложно получить данные из нескольких источников и низкая безопасность данных.

__Пример хранилища данных документов:__

1. Управление контентом. Платформы для размещения блогов или видео. При использовании документной базы каждая сущность в приложении может храниться как отдельный документ. 
2. MongoDB, RethinkDB, DocumentDB/

**Графовые БД** управляют сведениями двух типов: узлами и ребрами, создают отношения между сложновзаимосвязанными объектами. Узлы в этом случае представляют сущности, а ребра определяют связи между ними. Узлы и грани имеют свойства, которые предоставляют сведения о конкретном узле или грани, примерно как столбцы в реляционной таблице. Грани могут иметь направление, указывающее на характер связи. Помогают давать пользователям рекомендации в режиме реального времени.

**Плюсы:**

1. Высокая производительность и адаптивность.

2. Четкая взаимосвязь между сущностями.

3. Итоги в режиме реального времени.

**Минусы:**
1. Отсутствие стандартов в языке запросов.

2. Не подходят для работы с тразакциями.


__Пример данных в хранилище данных графов:__
1. Индив. подборки в музыкальном магазине или  списки товаров в МП на основе избранного. Нетфликс, ебэй.


**Колоночная БД** 

Эти БД имеют свои столбцы и строки, но информация записывается в колонки. Колонки между собой не связаны, поэтому удаление или добавление новых свойств не затрагивает остальную систему. Отсутствие заранее заданной схемы позволяет хранить в этих NoSQL-базах записи без чёткой структуры.

В традиционной СУБД при выполнении запроса сканируется вся таблица, а информация из всей строки извлекается целиком. В колоночных БД выгружаются только необходимые значения, поскольку поиск ведётся по отдельным столбцам. Такой подход колоночных NoSQL баз к хранению информации позволяет быстро получать данные из больших таблиц для анализа. А возможность сильного сжатия данных экономит много места на диске.
Применяются для анализа большого объема данных.

**Плюсы:**
1. Позволяет делает сложные выборки в больших таблицах.

2. Мгновенно менять структура больших таблиц.

3. Быстро выплнять запросы.

4. Легко масштабирует и модифицирует структуру.

**Минусы:**
1. Медленная запись.

2. Не поддерживает транзакции.

3. имеет некоторые ограничения для разрабов.


 **Примеры**
1. ClickHouse Яндекс, Snowflake, Amazon Redshift.
2. ClickHouse, Cassandra.

**Данными временных рядов** называются наборы значений, которые упорядочены по времени. Соответственно хранилища данных временных рядов оптимизированы для хранения данных именно такого типа. Хранилища данных временных рядов должны поддерживать очень большое число операций записи, так как обычно в них в режиме реального времени собирается большой объем данных из большого количества источников. Эти хранилища также хорошо подходят для хранения данных телеметрии. Например, для сбора данных от датчиков Интернета вещей или счетчиков в приложениях или системах. Обновления в таких базах данных выполняются редко, а удаление чаще всего является массовой операцией. Временные ряды - все, что можно измерить временными отрезками. Данные можно запросить, построить, провнализировать, найти закономерность и связь.

**Плюсы:**
1. Дает четкое представление об измнениях с течением времени.
2. Помогает быстро найти где именно произошли изменения.

3. Подходят для прогнозирования.

**Минусы:**
1. чувствительны ко врмеени, требует максимально скорого выполнения.

2. Требуется коррелирование данных из разных источников

3. Такие данные, как правило, имеют большой объем.

__Пример данных временных рядов:__

1. Prometheus, RRDtools.
2.  OpenTSDB, TimescaleDB.

## 4. NewSQL базы данных

**NewSQL (англ. новый SQL)** — класс современных реляционных СУБД, стремящихся совместить в себе преимущества NoSQL и транзакционные требования классических баз данных.

**Преимущества NewSQL**:

- приложения взаимодействуют в основном посредством SQL;  
- транзакции полностью поддерживают требования ACID;  
- при управлении не применяется механизм блокировок – таким образом, исключается вероятность конфликта между записываемыми и считываемыми в реальном времени данными;  
- архитектура shared—nothing, при которой каждой узел системы отвечает за свой набор данных, являясь независимым и самодостаточным, подразумевает легкую и быструю масштабируемость; Разделение масштабирует базу данных на единицы. Запросы выполняются на многих осколках и объединяются в один результат.  
- производительность узла СУБД на NewSQL намного выше, чем у традиционных РСУБД;  
- скорость отклика системы в 50 раз выше, чем у традиционных РСУБД.  
- Хранение в оперативной памяти и обработка данных обеспечивают быстрые результаты запросов.  
- Вторичная индексация ускоряет обработку запросов и поиск информации.  
- Высокая доступность благодаря механизму репликации базы данных.  
- Встроенный механизм аварийного восстановления обеспечивает отказоустойчивость и минимизирует время простоя.  
- Многие системы предлагают кластеризацию в стиле NoSQL с более традиционными моделями данных и запросов. 
 

**Недостатки NewSQL**:

- В настоящее время большинство баз данных NewSQL либо являются проприетарным программным обеспечением, либо применяются только для определенных сценариев, что значительно ограничивает распространение и принятие новой технологии.  
- NewSQL не однороден. Например, SAP HANA может легко справляться с низкими и средними транзакционными рабочими нагрузками, но не использует встроенную кластеризацию, MemSQL полезен для кластерной аналитики, но показывает плохую согласованность транзакций ACID и т. д.   
- Никакие системы NewSQL не являются настолько универсальными, как традиционные системы SQL.  
- Архитектуры в памяти могут не подходить для томов, превышающих несколько терабайт.  
- Предлагает лишь частичный доступ к богатому инструментарию традиционных систем SQL.
  

**Примеры**: Clustrix. NuoDB, Trafodion


## 5. Многомодельные базы данных

**Многомодельная база данных**  - это система управления базами данных, предназначенная для поддержки нескольких моделей данных в рамках единого интегрированного бэкэнда

**Преимущества многомодельной базы данных**:

- Данные организованы в виде упорядоченных многомерных массивов: гиперкубов (все хранимые в базе данных ячейки должны иметь одинаковую мерность, то есть находиться в максимально полном базисе измерений) и/или витрин данных, представляющих собой предметно-ориентированные подмножества хранилища данных, спроектированные для удовлетворения нужд отдельной группы (сообщества) пользователей и удовлетворяющие требованиям защиты от несанкционированного доступа в организации;   
- Данные, организованные в виде упорядоченных многомерных массивов, обеспечивают более быструю реакцию на запросы сведений за счет того, что обращения поступают к относительно небольшим блокам данных, необходимых для конкретной группы пользователей.   
- В случае многомерных баз данных, как правило, не требуется указание на то, по каким реквизитам (группам реквизитов) требуется индексация данных.   
- Многомодельная база данных денормализована, содержит заранее агрегированные показатели и обеспечивает оптимизированный доступ к запрашиваемым ячейкам.  
- Многомерные СУБД легко справляются с задачами включения в информационную модель разнообразных встроенных функций.  
- Общая простота системы, что позволяет осуществлять быстрое встраивание технологий многомодельных СУБД в приложения.   
- Относительно низкая общая стоимость владения, а также быстрый возврат инвестиций;  
- Согласованность данных между моделями за счет единой серверной системы  
- Динамичная среда с использованием различных типов данных на одной платформе  
- Отказоустойчивость, из-за ACID-совместимости  
- Подходят для сложных проектов с множественным представлением данных  
- Способны загружать в хранилище различные форматы данных, такие как CSV (включая графический, реляционный), JSON без каких-либо дополнительных усилий.  
- Могут использовать унифицированный язык запросов, такой как AQL, Orient SQL, SQL / XML, SQL / JSON, для извлечения коррелированных многомодельных данных, таких как graph-JSON-key/value, XML-relational и JSON-relational на единой платформе.  
- Способны поддерживать многомодельные транзакции ACID в автономном режиме. 
 
**Недостатки многомодельной базы данных**:

- Необходимость привлечения высококвалифицированных программистов для малейших изменений структуры базы данных.  
- Невозможность для конечного пользователя самостоятельно анализировать данные в порядке, не предусмотренном программистами.  
- Отсутствуют единые стандарты на интерфейс, языки описания и манипулирования данными.  
- Не поддерживают репликацию данных, наиболее часто используемую в качестве механизма загрузки.  
- Сложность МСУБД, из-за чего с ними трудно работать   
- Модель БД все еще развивается и не имеет окончательной формы  
- Ограниченная доступность различных методов моделирования  
- Не подходит для более простых проектов или систем  

**Примеры:** MarkLogic Server, ArangoDB, OrientDB

